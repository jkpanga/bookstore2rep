package com.boomi.connector.bookStoreProject;

import com.boomi.connector.api.GetRequest;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.util.BaseGetOperation;

public class BookStoreProjectGetOperation extends BaseGetOperation {

    protected BookStoreProjectGetOperation(BookStoreProjectConnection conn) {
        super(conn);
    }

	@Override
	protected void executeGet(GetRequest request, OperationResponse response) {
		// TODO Auto-generated method stub
	}

	@Override
    public BookStoreProjectConnection getConnection() {
        return (BookStoreProjectConnection) super.getConnection();
    }
}