package com.boomi.connector.bookStoreProject;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;

public class BookStoreProjectConnector extends BaseConnector {

    @Override
    public Browser createBrowser(BrowseContext context) {
        return new BookStoreProjectBrowser(createConnection(context));
    }    

    @Override
    protected Operation createGetOperation(OperationContext context) {
        return new BookStoreProjectGetOperation(createConnection(context));
    }

    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        return new BookStoreProjectExecuteOperation(createConnection(context));
    }
   
    private BookStoreProjectConnection createConnection(BrowseContext context) {
        return new BookStoreProjectConnection(context);
    }
}