package com.boomi.connector.bookStoreProject;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;

public class BookStoreProjectExecuteOperation extends BaseUpdateOperation {

	protected BookStoreProjectExecuteOperation(BookStoreProjectConnection conn) {
		super(conn);
	}

	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		// TODO Auto-generated method stub
	}

	@Override
    public BookStoreProjectConnection getConnection() {
        return (BookStoreProjectConnection) super.getConnection();
    }
}