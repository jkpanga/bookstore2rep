package com.boomi.connector.bookStoreProject;

import java.util.Collection;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.util.BaseBrowser;

public class BookStoreProjectBrowser extends BaseBrowser {

    protected BookStoreProjectBrowser(BookStoreProjectConnection conn) {
        super(conn);
    }

	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId,
			Collection<ObjectDefinitionRole> roles) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectTypes getObjectTypes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
    public BookStoreProjectConnection getConnection() {
        return (BookStoreProjectConnection) super.getConnection();
    }
}